import org.junit.Assert;
import org.junit.Test;

public class ConstantTest {
    @Test
   public void dummyFileTest(){
        String actual=(Constant.LOREMIPSUM);
        String expected="files/dummy.txt";
        Assert.assertEquals(expected,actual);
    }
    @Test
   public void faktaFileTest(){
        String actual=(Constant.BOHEMIANRAPSODY);
        String expected="files/FAKTA.txt";
        Assert.assertEquals(expected,actual);
    }
    @Test
   public void wikiFileTest(){
        String actual=(Constant.MRCOOL);
        String expected="files/wiki.txt";
        Assert.assertEquals(expected,actual);
    }
}
