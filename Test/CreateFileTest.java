import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

class CreateFileTest {

    @Test
    void contentCheck() throws IOException { //Testing to create a file and compair the content
        ArrayList<String> pizza = new ArrayList<>();
        pizza.add("banan");
        pizza.add("ananas");
        CreateFile textsave = new CreateFile();
        ReadFile readfile = new ReadFile();
        Assert.assertEquals(pizza, readfile.textToArray(textsave.createTextFile(pizza)));
    }

    @Test
    void emptyCheck() throws IOException {
        ArrayList<String> pizza = new ArrayList<>();
        CreateFile textsave = new CreateFile();
        textsave.createTextFile(pizza);

    }

    @Test
    void nullCheck() throws IOException {
        CreateFile textsave = new CreateFile();
        textsave.createTextFile(null);

    }

    @Test
    void nameCheck() throws IOException { //Testing if the method changes the name of file if file already created.
        ArrayList<String> pizza = new ArrayList<>();
        pizza.add("pepperoni");
        CreateFile textsave = new CreateFile();
        if (textsave.createTextFile(pizza).equals(textsave.createTextFile(pizza))){
            System.out.println("somethings wrong");
        }else System.out.println("everything ok");

    }
}