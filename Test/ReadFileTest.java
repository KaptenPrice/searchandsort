import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//ungef�r 3h
public class ReadFileTest {

    @Test
    public void fileReaderAlgoritmTest() throws IOException { //Testing fileReaderAlgoritm
        ReadFile readFile = new ReadFile();
        String input = "files/FAKTAtest.txt";
        String expected= "Aaa bbb ccc ddd eee Aaa bbb ccc ddd eee erik";
        String actual=readFile.fileReaderAlgorithm(input);
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void wordCleanerSizeAfterWordCleaner() throws IOException { //Testing textToArray small file
        ReadFile readFile = new ReadFile();
        String temp = "files/FAKTAtest.txt";
        int acutal = readFile.textToArray(temp).size();
        int expected = 11;
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void wordCleanerTestTabs() throws IOException {  //Testing textToArray bigger file
        ReadFile readFile = new ReadFile();
        String temp = Constant.DUMMYTEST;
        int expectedArray = 406;
        int actual = readFile.textToArray(temp).size();
        Assert.assertEquals(expectedArray, actual);
    }

    @Test
    public void nullCheckTest() throws IOException {
        ReadFile readFile = new ReadFile();
        readFile.fileReaderAlgorithm(null);
    }
    @Test
    public void TestIfEmptyArr() throws IOException {
        ReadFile rf = new ReadFile();
        rf.fileReaderAlgorithm("files/tomt.txt");
    }
}