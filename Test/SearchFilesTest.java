import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

class SearchFilesTest extends SortFiles {


    @Test
    void countWordsTest() throws IOException { //Simple test of binarySearch via the driver
        SearchFiles sf = new SearchFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Aaa");
        arrayList.add("Aaa");
        arrayList.add("Bbb");
        int actual = sf.searchDriver(arrayList, "aaa");
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }

    @Test
    void ifArrayEmpty() throws IOException {
        SearchFiles sf = new SearchFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        int actual = sf.searchDriver(arrayList, "aaa");
        Assert.assertEquals("actual is " + actual, -1, -1);
    }

    @Test
    void nullCheckTest() throws IOException {
        SearchFiles sf = new SearchFiles();
        int actual = sf.searchDriver(null, "aaa");
        Assert.assertEquals("No data find", 0, actual);

    }

    @Test
    void chekMixArrayTest() throws IOException { //Testing to change between 2 arrayFiles
        SearchFiles sf = new SearchFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("apor");
        arrayList.add("apor");
        arrayList.add("apor");
        arrayList.add("Aaa");
        arrayList.add("Aaa");
        arrayList.add("Aaa");
        arrayList.add("ccc");
        arrayList.add("ccc");
        arrayList.add("Bbb");
        arrayList.add("Bbb");
        arrayList.add("14-17");
        arrayList.add("8Zlatan");
        arrayList.add("m�nniskor");
        arrayList.add("m�nniskor");
        ArrayList<String> arrayList2 = new ArrayList<>(arrayList);
        SortFiles sortFiles = new SortFiles();
        sortFiles.sortAlgorithm(arrayList2);
        System.out.println(arrayList2.size());
        int actual = sf.searchDriver(arrayList2, "aaa");

        Assert.assertEquals(3, actual);
    }

    @Test
    void regretionTest() throws IOException { //Testing readfile, sortFile and searchFile at the same time
        ReadFile readFile = new ReadFile();
        SortFiles sortFiles = new SortFiles();
        SearchFiles searchFiles = new SearchFiles();
        ArrayList<String> incomingArr = readFile.textToArray(Constant.MRCOOL);
        ArrayList<String> sortedArr = sortFiles.sortAlgorithm(incomingArr);
        String target = "var";
        int expected = 4;
        int actual = searchFiles.searchDriver(sortedArr, target);
        Assert.assertEquals("found target", expected, actual);
    }
    //Testing to search in big amount of words by Collection.frekvensy
    @Test
    public void collectionFrekvensySearch() {
        ArrayList<String> testArr = new ArrayList<>();
        String temp = "";
        String s = "s";
        for (int x = 0; x < 20; x++) {
            temp += s;
            testArr.add(temp + " ");
            s += temp;
            testArr.add(s + " ");
        }
        long startTime = System.currentTimeMillis();
        int x = Collections.frequency(testArr, "sssssssss");
        System.out.println("Found so many times: " + x);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Collection frekvensy " + elapsedTime + " milliseconds");
        Assert.assertEquals(0, x);
    }
    //Testing to search in big amount of words by binarySearch
    @Test
    public void binarySearchTest() throws IOException {
        SearchFiles searchFiles = new SearchFiles();
        ArrayList<String> testArr = new ArrayList<>();
        String temp = "";
        String s = "s";
        for (int x = 0; x < 20; x++) {
            temp += s;
            testArr.add(temp + " ");
            s += temp;
            testArr.add(s + " ");
        }
        long startTime = System.currentTimeMillis();
        int x = searchFiles.searchDriver(testArr, "sssssssss");
        System.out.println("Found so many times: " + x);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Collection frekvensy " + elapsedTime + " milliseconds");
        Assert.assertEquals(0, x);
    }

    @Test
    public void captitalInputTest() throws IOException { //testing capital letters when array contains lower case letters
        SearchFiles searchFiles = new SearchFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("aaa");
        arrayList.add("aaa");
        arrayList.add("bbb");
        int actual = searchFiles.searchDriver(arrayList, "AAA");
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void lowerCaseInputTest() throws IOException { //Testing lowercase against upper case
        SearchFiles searchFiles = new SearchFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("AAA");
        arrayList.add("AAA");
        arrayList.add("BBB");
        int actual = searchFiles.searchDriver(arrayList, "aaa");
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }

}