import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class SortFilesTest {

    @Test
    public void nullCheck() {
        SortFiles sortFiles = new SortFiles();
        sortFiles.sortAlgorithm(null);
    }

    @Test
    public void testEmptyArr() {
        SortFiles sortFiles = new SortFiles();
        ArrayList<String> arr = new ArrayList<>(0);
        sortFiles.sortAlgorithm(arr);
    }

    //testing quickSort via driver "SortingAlgoithm"
    @Test
    public void testSimpleArray() {
        SortFiles sortFiles = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        testArr.add("B");
        testArr.add("A");
        testArr.add("C");
        sortFiles.sortAlgorithm(testArr);
        testArr.replaceAll(String::toUpperCase);
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }
    }

    @Test
    public void testInts() {
        SortFiles qs = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        testArr.add("1");
        testArr.add("2");
        qs.sortAlgorithm(testArr);
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }
    }

    @Test
    public void testDoubles() {
        SortFiles qs = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        testArr.add("1,35");
        testArr.add("1,50");
        testArr.add("1,73");
        testArr.add("2,89");
        qs.sortAlgorithm(testArr);
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }
    }
//Manual test of array by printOut
    @Test
    public void testMixedArray() {
        SortFiles qs = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        testArr.add("A");
        testArr.add("1");
        testArr.add("1,54");
        testArr.add("2,99");
        qs.sortAlgorithm(testArr);
        System.out.println("TEST MIXED ARRAY:");
        printResult(testArr);
    }

    @Test
    public void testFullArray() {
        SortFiles sortFiles = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        testArr.add("Erik");
        testArr.add("bertil");
        testArr.add("bertil");
        testArr.add("Adam");
        testArr.add("Adam");
        testArr.add("Caesar");
        testArr.add("Caesar");
        testArr.add("David");
        testArr.add("David");
        sortFiles.sortAlgorithm(testArr);
        testArr.replaceAll(String::toUpperCase);
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }

        printResult(testArr);
    }

    //Testing quickSort with 10 000 000 words
    @Test
    public void testElapsedTimeQuickSort() {
        SortFiles qs = new SortFiles();
        ArrayList<String> testArr = new ArrayList<>();
        //String[] tempArr = new String[10000000];
        for (int x = 0; x < 10; x++) {
            testArr.add(String.valueOf(x));
        }
        long startTime = System.currentTimeMillis();
        qs.sortAlgorithm(testArr);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("QuickSort " + elapsedTime + " milliseconds");
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }
        // printResult(testArr);
        assert (true);
    }

    //Testing quickSort with 10 000 000 words
    @Test
    public void standardSortTest() {

        ArrayList<String> testArr = new ArrayList<>();
        for (int x = 0; x < 10000000; x++) {
            testArr.add(String.valueOf(x));
        }
        long startTime = System.currentTimeMillis();
        Collections.sort(testArr);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("StandardSort " + elapsedTime + " milliseconds");
        if (!validate(testArr)) {
            fail("Shouldnt happen");
        }
        assert (true);

    }

    //Simple printing method
    private void printResult(ArrayList<String> testArr) {
        for (String s : testArr) {
            System.out.println(s);
        }
        System.out.println();
    }

    //Loop throw incoming array and check if array is sorted
    private boolean validate(ArrayList<String> testArr) {
        testArr.replaceAll(String::toUpperCase);
        for (int i = 0; i < testArr.size() - 1; i++) {

            if (testArr.get(i).charAt(0) > testArr.get(i + 1).charAt(0)) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void sortingAlgorithmFullTest() throws IOException {
        SortFiles sortFiles = new SortFiles();
        ArrayList<String> actualArr = new ArrayList<>();
        actualArr.add("Erik");
        actualArr.add("Bertil");
        actualArr.add("Adam");
        actualArr.add("adam");
        actualArr.add("Caesar");
        actualArr.add("David");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Adam");
        expected.add("adam");
        expected.add("Bertil");
        expected.add("Caesar");
        expected.add("David");
        expected.add("Erik");
        Assert.assertEquals(expected, sortFiles.sortAlgorithm(actualArr));
    }


}