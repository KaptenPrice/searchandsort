import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
//System testing all methods by on object
public class SystemTesting {
    @Test
    public void testAllFunctions() throws IOException {
        ReadFile readFile = new ReadFile(); //Testing readfile
        ArrayList<String> actual = readFile.textToArray(Constant.FAKTATEST);
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Aaa");
        expected.add("bbb");
        expected.add("ccc");
        expected.add("ddd");
        expected.add("eee");
        expected.add("Aaa");
        expected.add("bbb");
        expected.add("ccc");
        expected.add("ddd");
        expected.add("eee");
        expected.add("erik");
        Assert.assertEquals(expected, actual);
        SortFiles sortFiles = new SortFiles();        //Testing sortFiles
        ArrayList<String> afterSortArr = sortFiles.sortAlgorithm(actual);
        Collections.sort(expected);
        Assert.assertEquals(expected, afterSortArr);
        SearchFiles sf = new SearchFiles();        //testing searchFiles
        int actualIndexOfSearch = sf.searchDriver(afterSortArr, "aaa");
        int expectedOfSearch = 2;
        Assert.assertEquals(expectedOfSearch, actualIndexOfSearch);
        Hubb hubb = new Hubb();        //Testing highest occurrence
        ArrayList<String> occurranceArr = new ArrayList<>();
        occurranceArr.add(Constant.DUMMYTEST);
        occurranceArr.add(Constant.WIKITEST);
        occurranceArr.add(Constant.FAKTATEST);
        int actualOccurrance = hubb.highestOccurrance(occurranceArr, "aaa");
        int expectedOccurance = 2;
        Assert.assertEquals(expectedOccurance, actualOccurrance);
    }
}
