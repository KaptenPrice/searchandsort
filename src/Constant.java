public class Constant {
//Constant variables initiated here
    public static final String BOHEMIANRAPSODY =("files/FAKTA.txt");
    public static final String MRCOOL =("files/wiki.txt");
    public static final String LOREMIPSUM =("files/dummy.txt");
    public static final String FAKTATEST =("files/FAKTAtest.txt");
    public static final String DUMMYTEST =("files/dummytest.txt");
    public static final String WIKITEST =("files/wikitest.txt");
}
