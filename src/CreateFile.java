import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CreateFile {

    //gives file unique names
    //Create a name for a file and creates a file with that name
    //Check if file was created then writes "textToSave" in text file
    //returns name of file created
    //if file isn't created it's because the name was taken
    //if so it changes name and loops back again


    public String createTextFile(ArrayList<String> textToSave) throws IOException {

        int txtID = 0;

        if (textToSave == null || textToSave.isEmpty()) {
            System.out.println("null/no input");
            return "";
        }

        while (true) {
            String fileName = "files\\myTextFile" + txtID + ".txt";
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("\n"+fileName+" is created!");
            }

            else {
                txtID++;
                continue;
            }

            BufferedWriter bf = new BufferedWriter(new FileWriter(file));
            bf.write(String.valueOf(textToSave));
            bf.close();

            return fileName;
        }

    }
}
        


