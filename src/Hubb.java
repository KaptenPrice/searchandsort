import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Hubb {
//Objects of our classes
    private ReadFile readFile = new ReadFile();
    private CreateFile createFile = new CreateFile();
    private SortFiles sortFiles = new SortFiles();
    private SearchFiles searchFiles = new SearchFiles();

    //switch menu
    public void menu() {
        //ArrayList containing our  .txt filepaths
        ArrayList<String> textFilesArray = new ArrayList<>();
        textFilesArray.add(Constant.BOHEMIANRAPSODY);
        textFilesArray.add(Constant.LOREMIPSUM);
        textFilesArray.add(Constant.MRCOOL);

        while (true) {
            System.out.println("\n[1]Search and compare" + "\n[2]Sort and save" + "\n[3]Exit ");
            try {
                Scanner sc = new Scanner(System.in);
                String choice = sc.nextLine();
                if (choice.contains(" ")) {
                    continue;
                }
                switch (choice) {
                    //For search method
                    case "1":
                        System.out.println("Choose file to search: \n[1]Bohemian Rapsody\n[2]Lorem Ipsum\n[3]Mr Cool\n[4]All files\n[5]Back to main menu");
                        String choiceFile = sc.nextLine();
                        if (choiceFile.contains(" "))
                            continue;
                        switch (choiceFile) {
                            case "1":
                                System.out.println("Please enter the word you want to search for.");
                                String searchedWord = sc.nextLine();
                                //Searchmethod on .txtfiles
                                //Uses searchDriver for BinarySearchalgorithm
                                //Uses sortAlgorithm for sorting the list
                                //Uses textToArray to make txt file to arraylist with words.
                                searchFiles.searchDriver(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(0))), searchedWord);
                                break;
                            case "2":
                                System.out.println("Please enter  the word you want to search for.");
                                String searchedWord1 = sc.next();
                                searchFiles.searchDriver(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(1))), searchedWord1);
                                break;
                            case "3":
                                System.out.println("Please enter the word you want to search for.");
                                String searchedWord2 = sc.next();
                                searchFiles.searchDriver(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(2))), searchedWord2);
                                break;
                                //For all files
                            case "4":
                                System.out.println("Please enter the word you want to search for.");
                                searchedWord = sc.next();
                                highestOccurrance(textFilesArray, searchedWord);
                                break;
                            case "5":
                                System.out.println("Back to menu");
                            default:
                                System.out.println("Choose from available options 1-5");
                        }
                        break;
                        //Sortmethod
                    case "2":
                        System.out.println("Choose file to sort and save: \n[1]Bohemian Rapsody\n[2]Lorem Ipsum\n[3]Mr Cool\n[4]All files\n[5]Back to main menu");
                        String choiceFileToSort = sc.nextLine();
                        if (choiceFileToSort.contains(" ")) {
                            continue;
                        }
                        switch (choiceFileToSort) {
                            case "1":
                                //Sortmethod on .txtfiles.
                                // Uses createTextFile for saving and creating new file.
                                // Uses SortAlgorithm for sorting the file.
                                // Uses  textToArray to turn txtFile to Arraylist.
                                createFile.createTextFile(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(0))));
                                break;
                            case "2":
                                createFile.createTextFile(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(1))));
                                break;
                            case "3":
                                createFile.createTextFile(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(2))));
                                break;
                            case "4":
                                //loop to run through all of textFilesArray
                                for (String s : textFilesArray) {
                                    createFile.createTextFile(sortFiles.sortAlgorithm(readFile.textToArray(s)));
                                }
                                break;
                            case "5":
                                System.out.println("Back to menu");
                                //if input is not between 1-5
                            default:
                                System.out.println("Choose from available options 1-5");
                                break;
                        }
                        break;
                        //exit program
                    case "3":
                        System.exit(0);
                        break;
                        //if input is not between 1-3
                    default:
                        System.out.println("Choose from available options 1-3");
                }
                //Catching if input is invalid
            } catch (InputMismatchException e) {
                System.out.println("Choose from menu and try with integer.");
                //if the file is not found
            } catch (FileNotFoundException fe) {
                System.out.println("File not found");
                //exception for readingfiles
            } catch (IOException e) {
                e.printStackTrace();
                //if you are looking outside a array or pointing to something that doesn't exist
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                System.out.println(e);
            }
        }
    }
//method that finds the highest occurring when comparing different .txt files
    int highestOccurrance(ArrayList<String> textFilesArray, String searchedWord) throws IOException {
        int highestCompare = 0;
        int highestOccurranceFile = -1;
        for (int i = 0; i < textFilesArray.size(); i++) {
            //the currentfile that is being searched
            int currentCompare = searchFiles.searchDriver(sortFiles.sortAlgorithm(readFile.textToArray(textFilesArray.get(i))), searchedWord);
            //if currentCompare is higher than highestCompare set currentCompare to highestCompare and save what file it was in.
            if (currentCompare > highestCompare) {
                highestCompare = currentCompare;
                highestOccurranceFile = i;
            }
        }
        //If there was a highest occurrence
        if (highestOccurranceFile != -1) {
            System.out.println("You searched for: '" + searchedWord + "' it was found: " + highestCompare + " times in '" + textFilesArray.get(highestOccurranceFile).substring(6, 11) + "'");
            return highestCompare;
            //if there searched word wasn't found
        } else {
            System.out.println("You searched for: " + searchedWord + " It was not found in any file ");
            return 0;
        }
    }
}