import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

 class ReadFile {
    /*The BufferedReader object takes a FileReader object as an input which contains all
    the information in the constantFile that needs to be read. Then we call the bufferReader method readLine()
    which uses the FileReader object to read the data from the file. When an instruction is given,
    the FileReader object reads 2 (or 4) bytes at a time and returns the data to the BufferedReader
     and the reader loopes until it hits '\n' or '\r\n'.
     Once a line is buffered, the reader waits until the instruction to buffer the next line is given.
    Meanwhile, The BufferReader object creates a special memory place (On the RAM), called "Buffer",
     and stores all the fetched data from the FileReader object.
     At the end we close the bufferReader and return file as a String.
     */
    String fileReaderAlgorithm(String constantFile) throws IOException {
        if (constantFile==null){
            System.out.println("No file path found");
            return null;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(constantFile));
        String line;String temp = "";
        while ((line = bufferedReader.readLine()) != null) {
            temp += line;
        }
        bufferedReader.close();
        return temp;
    }

    //Takes in .txt file and removes all spaces and special characters and adds each word to an arraylist.
    ArrayList<String> textToArray(String constantFile) throws IOException {
        //NullCheck
        if (constantFile == null) {
            System.out.println("No files to read, no words to declare");
            return null;
        }
        //TempArray that is used for storing the split String.
        ArrayList<String> tempArray = new ArrayList<>();
        try {
            // reading filePath and setting txtfile to a string.
            String temp = fileReaderAlgorithm(constantFile);
            assert temp != null;
            //Splits the string, removing special characters and spaces. adds every new word created as string to arraylist
            Collections.addAll(tempArray, temp.split("[^\\p{L}0-9']", temp.length()));
            //forloop that checks if there is any empty elements in array, if yes, remove
            for (int i = 0; i < tempArray.size(); i++) {
                if (tempArray.get(i).isBlank()) {
                    tempArray.remove(i);
                    i--;
                }
            }
            System.out.println("Amount of words: " + tempArray.size());
        } catch (FileNotFoundException fe) {
            System.out.println("No content to read");
        }
        //returns arraylist
        return tempArray;
    }
}