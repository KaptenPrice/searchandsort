import java.io.IOException;
import java.util.ArrayList;

public class SearchFiles {

    //nullcheck
    //turns all letters to uppercase to make it case insensitive
    //ask countOccurrenceOfTarget method how many times "target" appears in arr2
    //returns that number
    public int searchDriver(ArrayList<String> arr, String searchedWord) {

        if (arr == null) {
            return 0;
        }
        arr.replaceAll(String::toUpperCase);
        int c = findSearchedWordInArray(arr, searchedWord.toUpperCase());
        if (c<0){
            c=0;
        }
        System.out.println(searchedWord + " occurs " + c + " times");
        return c;
    }


    //nullcheck
    // "i" becomes the first occurrence of the "searchword" in the arraylist
    // "i" becomes -1 if the "searchword" doesn't occur, returns -1 which handled as a 0
    // "j" becomes the last occurrence of the "searchword" in the arraylist
    // returns the difference (aka the amount of times it occurs)
    private int findSearchedWordInArray(ArrayList<String> arr, String searchedWord) {

        if (arr==null||searchedWord==null){
            return 0;
        }
        System.out.println("");

        int i=first(arr, searchedWord);

        if (i == -1) return i;

        int j=last(arr, searchedWord);

        return (j - i) + 1;
    }


    //finds middle of arraylist
    // "res" becomes a number based on "searchword" location in the array compared to "middle"
    // if "res" is 0, "searchword" and "middle" are the same, it then checks if the word before "middle" is the same
    // if so it checks if the word before that also is the same and loops like that to return the first time the word occurs
    // if "res" is positive, "searchword" is later than "middle"
    // if "res" is negative, "searchword" is earlier than "middle"
    // if there are no words like "searchword, returns -1 which handled as a 0

    private int first(ArrayList<String> arr, String searchedWord) {

        int left = 0;
        int right = arr.size() - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;
            int res = searchedWord.compareTo(arr.get(middle));

            if (res == 0) {
                while (true) {
                    if (middle == 0) {
                        return middle;
                    }
                    if (0 == searchedWord.compareTo(arr.get(middle - 1)) && middle > 0)
                        middle--;
                    else return middle;

                }
            }
            if (res > 0)
                left = middle + 1;
            else
                right = middle - 1;
        }
        return -1;
    }

    //last method is same as first but checks the word after instead of before when it finds a match
  private int last(ArrayList<String> arr, String searchedWord) {

        int left = 0;
        int right = arr.size() - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;
            int res = searchedWord.compareTo(arr.get(middle));

            if (res == 0) {
                while (true) {
                    if (0 == searchedWord.compareTo(arr.get(middle + 1)) && middle < right)
                        middle++;
                    else return middle;
                }
            }
            if (res > 0)
                left = middle + 1;
            else
                right = middle - 1;
        }
        return -1;
    }
}