import java.util.ArrayList;

public class SortFiles {

//Driver calls QuickSort method and prints out the sortedarray, puts the sorted words in a Arraylist and returns the array
    public ArrayList<String> sortAlgorithm(ArrayList<String> array) {
        if (array==null) {
            System.out.println("Empty list");
            return null;
        }
        quickSort(array, 0, array.size() - 1);
        for (String i : array) {
            System.out.print(i + " ");
        }
        return array;
    }
//Quicksort metod calls it self Recursively, Command and conquer via exchange depending on if-condition in partition is true or false
    private void quickSort(ArrayList<String> array, int first, int last) {
        int pivotPoint;
//as long as first is less then last.
        if (first < last) {
            pivotPoint = partition(array, first, last); //PivotPoint is the value of
            //First call to quickSort with the array and incomming first value but -decreased pivoPoint as int last
            quickSort(array, first, pivotPoint - 1);
            //Then call quickSort with pivo increased as first and incomming last as last
            quickSort(array, pivotPoint + 1, last);
        }
    }
  //  Arrange the array so that all elements with values less than the pivot come after the pivot in the end of the list,
  //  compare pivotstrings value to array(round)positions value,
  //  aslong as pivostrings value is higher then or equal to the pivoStrings value it will ignore it.
    //if the value is less then pivos value->exchange metod with firsts values.
   private int partition(ArrayList<String> array, int first, int last) {
        String pivotValue;
        int endOfLeftList;
        int mid = (first + last) / 2; //init the middle of array
        exchange(array, first, mid); //sends the array and int first and last right no to exchange method
        pivotValue = array.get(first); //pivotValue is the value of word from array position with value of int first
        endOfLeftList = first; //End of the list is now equal to int first
        //as long as round is less then arrays last position or equal to it
        for (int round = first + 1; round <= last; round++) {
            //
            if (array.get(round).compareToIgnoreCase(pivotValue) < array.get(first).compareToIgnoreCase(pivotValue)) {
                endOfLeftList++; //Increase the value
                exchange(array, endOfLeftList, round); //array lenght, value of end of the list and loops round number
            }
        }
        exchange(array, first, endOfLeftList);

        return endOfLeftList;
    }
//exchange the words in array
    private static void exchange(ArrayList<String> array, int a, int b) {
        String temp;
        temp = array.get(a);//temp is now array element a
        array.set(a, array.get(b)); //array postition a is now the element from position b
        array.set(b, temp); //Array postition b is now the word from array position a
    }


}